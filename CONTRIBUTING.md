# Contributing to co2logserver

See [GitLab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html).

## Code Style

Code should be formatted with [`black`](https://github.com/ambv/black) and
follow [PEP8](https://www.python.org/dev/peps/pep-0008/).

Be sure to have run the following commands before pushing commits. Otherwise,
the CI pipeline will only pass with warnings.

```bash
pip3 install --user black pycodestyle autopep8
# optionally run autopep8
autopep8 -arij0 .
# format all Python files in black style
black -l79 .
# check PEP8 compatibility
pycodestyle .
```

## Development

The following might only be interesting for developers

### Installation

```bash
# in "editable" mode for development
pip3 install --user -e .
```

### Testing

```bash
# Run all tests
./setup.py test
```

```bash
# install coverage
pip3 install --user coveralls
# Run all tests and determine a test coverage
make coverage
```

### Versioning

-   `make increase-patch` to increase the patch version number
-   `make increase-minor` to increase the minor version number
-   `make increase-major` to increase the major version number


