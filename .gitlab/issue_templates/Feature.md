# New Feature

[comment]: # (Describe what the new feature should look like.)

# Benefits

[comment]: # (Describe what the advantages of this new feature are.)

# Downsides/Conflicts

[comment]: # (If you can imagine some downsides or conflicts that this new feature could introduce, you may describe them here.)

# Technical Requirements

[comment]: # (If possible, describe what technical requirements this new feature has. Does it need an external library? Does it require a substantial structural change? ...)

/label ~"feature request"
