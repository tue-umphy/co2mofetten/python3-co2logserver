# system modules
import sensemapi
import msgpack
import unittest
import datetime
import itertools
import random
import http.client
import os
import re
import socket
import time
import urllib.request
import sqlite3
import logging
import threading
import hashlib

# internal modules
import co2logserver
from co2logserver import app
from co2logserver.config import database, osem_account
from co2logserver.utils import *

from . import needs_osem_credentials

logger = logging.getLogger("servertest")

# external modules


class LogServerTest(unittest.TestCase):
    def setUp(self):
        self.setUpPre()
        self.dbfile = "{}.sqlite".format(self.__class__.__name__)
        app.config["CO2LOGSERVER_NAME"] = self.__class__.__name__
        app.config["CO2LOGSERVER_DB"] = self.dbfile
        app.config[
            "CO2LOGSERVER_OSEM_API"
        ] = sensemapi.paths.OPENSENSEMAP_API_TEST
        self.setUpServerSettings()
        app.logger.setLevel(logging.CRITICAL + 1)
        self.app = app.test_client()
        self.setUpPost()

    def setUpServerSettings(self):
        pass

    def setUpPre(self):
        pass

    def setUpPost(self):
        pass

    def tearDownPre(self):
        pass

    def tearDown(self):
        self.tearDownPre()
        self.remove_dbfile()
        app.config.from_object("co2logserver.config_default")

    def remove_dbfile(self):
        try:
            logger.debug("Removing test dbfile '{}'".format(self.dbfile))
            os.remove(self.dbfile)
        except FileNotFoundError:
            pass


class RootPageTest(LogServerTest):
    def test_root_get(self):
        response = self.app.get("/")
        self.assertIn(app.config["CO2LOGSERVER_NAME"], response.data.decode())


class RootPageNoUploadHelpTest(LogServerTest):
    def setUpPre(self):
        app.config["CO2LOGSERVER_UPLOAD_HELP"] = False

    def test_no_upload_help_link_if_config_says_no(self):
        page = self.app.get("/").data.decode()
        self.assertFalse(
            re.search(
                r"instructions.*upload.*here", page, re.IGNORECASE | re.DOTALL
            ),
            "Root page contains link to upload instructions "
            "Despite disabled in settings",
        )

    def test_no_upload_help_page(self):
        self.assertEqual(
            self.app.get("/uploadhelp").status_code,
            404,
            "There is an upload page despite disabled in settings",
        )


class UploadTest(LogServerTest):
    DATASET_N = 1
    SALT_CORRECT = "qweroiqweurasdfyxcuioqoweiur"
    SALT_WRONG = "asdf"

    @classmethod
    def random_dataset(cls, n=DATASET_N):
        return {
            "time_utc": [
                datetime.datetime.utcnow().strftime("%F %T") for i in range(n)
            ],
            "sensor_id": [random.randint(0, 100) for i in range(n)],
            "co2": [random.randint(0, 10000) for i in range(n)],
            "temp1": [(random.random() - 0.5) * 20 for i in range(n)],
            "temp2": [(random.random() - 0.5) * 20 for i in range(n)],
            "hum": [random.randint(0, 100) for i in range(n)],
        }

    def assert_database_equals(self, dataset, msg=None):
        with app.app_context():
            cursor = database().tablecursor
            header = header_from_cursor(cursor)
            database_data = table2dict(header, cursor)
            # don't compare columns that are not in the dataset
            for col in set(header) - set(dataset.keys()):
                database_data.pop(col, None)
            self.assertDictEqual(dataset, database_data, msg)

    def assert_database_count_equals(self, count, msg=None):
        with app.app_context():
            self.assertEqual(
                len(database().tablecursor.fetchall()), count, msg
            )

    def upload_data_string(self, data, headers, salt=None, algorithm="md5"):
        if salt:
            checksum = salted_checksum(data, salt, algorithm)
            headers.update({"Content-{}-Salted".format(algorithm): checksum})
        response = self.app.post("/upload", data=data, headers=headers)
        return response

    def upload_csv(self, dataset, salt=None):
        return self.upload_data_string(
            data=table2csv(
                dataset.keys(), itertools.zip_longest(*dataset.values())
            ),
            headers={"Content-Type": "text/csv"},
            salt=salt,
        )

    def upload_json(self, dataset, salt=None):
        return self.upload_data_string(
            data=json.dumps(dataset),
            headers={"Content-Type": "application/json"},
            salt=salt,
        )

    def upload_form(self, dataset, salt=None):
        payload = dictlist2form(listdict_to_dictlist(dataset))
        return self.upload_data_string(
            data=payload,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            salt=salt,
        )

    def upload_msgpack(self, dataset, salt=None):
        payload = msgpack.packb(dataset)
        return self.upload_data_string(
            data=payload,
            headers={"Content-Type": "application/msgpack"},
            salt=salt,
        )


class UploadTestMiscellaneous(UploadTest):
    def test_config_accept_no_new_columns_dont_allow_empty(self):
        app.config["CO2LOGSERVER_ACCEPT_NEW_COLUMN"] = lambda x: False
        app.config["CO2LOGSERVER_ALLOW_EMPTY_ROWS"] = False
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_json(dataset, salt=None)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["saved-in-db"], 0)
        self.assert_database_equals({})
        self.assert_database_count_equals(0)

    def test_config_accept_no_new_columns_allow_empty(self):
        app.config["CO2LOGSERVER_ACCEPT_NEW_COLUMN"] = lambda x: False
        app.config["CO2LOGSERVER_ALLOW_EMPTY_ROWS"] = True
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_json(dataset, salt=None)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["saved-in-db"], 1)
        self.assert_database_equals({})
        self.assert_database_count_equals(1)

    def test_config_accept_specific_new_columns(self):
        app.config["CO2LOGSERVER_ACCEPT_NEW_COLUMN"] = re.compile(
            "^internet$"
        ).match
        response = self.upload_json(
            {"time": [1], "internet": [1337]}, salt=None
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json["saved-in-db"], 1)
        self.assert_database_equals({"internet": [1337]})
        self.assert_database_count_equals(1)


class UploadTestNoUploadAuthentication(UploadTest):
    def setUpPre(self):
        app.config["CO2LOGSERVER_UPLOAD_REQUIRES_AUTH"] = False

    def test_upload_json_sql_injection(self):
        response = self.upload_csv({"'": ";"}, salt=None)
        self.assertEqual(response.status_code, 400)
        self.assert_database_equals({})

    def test_upload_csv_without_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_csv(dataset, salt=None)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_csv_with_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_csv(dataset, salt=self.SALT_WRONG)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_json_without_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_json(dataset, salt=None)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_json_with_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_json(dataset, salt=self.SALT_WRONG)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_form_without_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_form(dataset, salt=None)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_form_with_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_form(dataset, salt=self.SALT_WRONG)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_msgpack_without_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_msgpack(dataset, salt=None)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_msgpack_with_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_msgpack(dataset, salt=self.SALT_WRONG)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)


class UploadTestUploadAuthMatchMinOneSalt(UploadTest):
    def setUpPre(self):
        app.config["CO2LOGSERVER_UPLOAD_REQUIRES_AUTH"] = True
        app.config["CO2LOGSERVER_AUTH_MIN_MATCHING_CHECKSUMS"] = 1
        app.config["CO2LOGSERVER_CHECKSUM_SALTS"] = [self.SALT_CORRECT]

    # def setUpPost(self):
    #     app.logger.setLevel(logging.DEBUG)

    def test_upload_csv_without_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_csv(dataset, salt=None)
        self.assertNotEqual(response.status_code, 200)
        self.assert_database_equals({})

    def test_upload_csv_wrong_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_csv(dataset, salt=self.SALT_WRONG)
        self.assertNotEqual(response.status_code, 200)
        self.assert_database_equals({})

    def test_upload_csv_correct_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_csv(dataset, salt=self.SALT_CORRECT)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_json_without_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_json(dataset, salt=None)
        self.assertNotEqual(response.status_code, 200)
        self.assert_database_equals({})

    def test_upload_json_wrong_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_json(dataset, salt=self.SALT_WRONG)
        self.assertNotEqual(response.status_code, 200)
        self.assert_database_equals({})

    def test_upload_json_correct_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_json(dataset, salt=self.SALT_CORRECT)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_form_without_checksum(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_form(dataset, salt=None)
        self.assertNotEqual(response.status_code, 200)
        self.assert_database_equals({})

    def test_upload_form_wrong_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_form(dataset, salt=self.SALT_WRONG)
        self.assertNotEqual(response.status_code, 200)
        self.assert_database_equals({})

    def test_upload_form_correct_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_form(dataset, salt=self.SALT_CORRECT)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_msgpack_correct_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_msgpack(dataset, salt=self.SALT_CORRECT)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(int(response.json["saved-in-db"]), self.DATASET_N)
        self.assert_database_equals(dataset)

    def test_upload_msgpack_wrong_salt(self):
        dataset = self.random_dataset(self.DATASET_N)
        response = self.upload_msgpack(dataset, salt=self.SALT_WRONG)
        self.assertNotEqual(response.status_code, 200)
        self.assert_database_equals({})


class OpenSenseMapUploadTest(UploadTest):
    def setUpPost(self):
        app.config["CO2LOGSERVER_OSEM_UPLOAD"] = True
        app.config["CO2LOGSERVER_OSEM_CACHE"] = False
        with app.app_context():
            account = osem_account()
            self.box_ids_before = set(account.get_own_boxes().by_id.keys())

    def tearDownPre(self):
        with app.app_context():
            account = osem_account()
            account.get_own_boxes()
            box_ids_after = set(account.boxes.by_id.keys())
            for box_id in box_ids_after - self.box_ids_before:
                account.delete_box(box_id, really=True)

    @needs_osem_credentials
    def test_upload_creates_new_box(self):
        dataset = {"sensortype_quantity_unit": [1000], "lat": [45], "lon": [9]}
        response = self.upload_json(dataset)
        with app.app_context():
            account = osem_account()
            # NOTE: Disabling the cache is necessary here, because somewhere
            # when uploading a new_box() - probably in `upload_json(dataset)`
            # some lines above - an incomplete box request (which does not yet
            # contain the measurements) is getting cached and would be returned
            # here with the cache enabled.
            # This is rather a bug in sensemapi, but this workaround does it
            # for now.
            with account.no_cache:
                account.get_own_boxes()
            new_ids = list(
                set(account.boxes.by_id.keys()) - set(self.box_ids_before)
            )
            self.assertEqual(
                len(new_ids),
                response.json.get("osem-new-boxes", 0),
                "Instead of {} new boxes as reported by the server, "
                "{} new boxes were created".format(
                    response.json.get("osem-new-boxes", 0), len(new_ids)
                ),
            )
            box_name = app.config["CO2LOGSERVER_OSEM_BOX_NAME_GENERATOR"](
                servername=app.config["CO2LOGSERVER_NAME"],
                box=app.config["CO2LOGSERVER_OSEM_BOX_COL_DEFAULT"],
            )
            box = account.boxes.by_name[box_name]
            self.assertEqual(
                box.current_lat,
                dataset["lat"][0],
                "Instead of {}, a latitude of {} was uploaded".format(
                    dataset["lat"][0], box.current_lat
                ),
            )
            self.assertEqual(
                box.current_lon,
                dataset["lon"][0],
                "Instead of {}, a longitude of {} was uploaded".format(
                    dataset["lon"][0], box.current_lon
                ),
            )
            sensor_title = app.config[
                "CO2LOGSERVER_OSEM_SENSOR_TITLE_GENERATOR"
            ](
                **app.config["CO2LOGSERVER_OSEM_COL_REGEX"]
                .search("sensortype_quantity_unit")
                .groupdict()
            )
            self.assertIn(
                sensor_title,
                box.sensors.by_title,
                "No sensor '{}' was created".format(sensor_title),
            )
            sensor = box.sensors.by_title[sensor_title]
            self.assertEqual(
                sensor.last_value,
                dataset["sensortype_quantity_unit"][0],
                "Instead of {}, a measurement of {} was uploaded".format(
                    dataset["sensortype_quantity_unit"][0], sensor.last_value
                ),
            )

    @needs_osem_credentials
    def test_ignored_columns_are_not_uploaded(self):
        app.config["CO2LOGSERVER_OSEM_COL_REDIRECT"] = re.compile(
            "wanted"
        ).search
        wanted_col = "wantedsensor_wantedquantity_wantedunit"
        ignored_col = "ignoredsensor_ignoredquantity_ignoredunit"
        dataset = {
            wanted_col: [1000],
            ignored_col: [1111],
            "lat": [45],
            "lon": [9],
        }
        response = self.upload_json(dataset)
        with app.app_context():
            account = osem_account()
            # NOTE: Disabling the cache is necessary here, because somewhere
            # when uploading a new_box() - probably in `upload_json(dataset)`
            # some lines above - an incomplete box request (which does not yet
            # contain the measurements) is getting cached and would be returned
            # here with the cache enabled.
            # This is rather a bug in sensemapi, but this workaround does it
            # for now.
            with account.no_cache:
                account.get_own_boxes()
            new_ids = list(
                set(account.boxes.by_id.keys()) - set(self.box_ids_before)
            )
            self.assertEqual(
                len(new_ids),
                response.json.get("osem-new-boxes", 0),
                "Instead of {} new boxes as reported by the server, "
                "{} new boxes were created".format(
                    response.json.get("osem-new-boxes", 0), len(new_ids)
                ),
            )
            box_name = app.config["CO2LOGSERVER_OSEM_BOX_NAME_GENERATOR"](
                servername=app.config["CO2LOGSERVER_NAME"],
                box=app.config["CO2LOGSERVER_OSEM_BOX_COL_DEFAULT"],
            )
            box = account.boxes.by_name[box_name]
            self.assertEqual(
                box.current_lat,
                dataset["lat"][0],
                "Instead of {}, a latitude of {} was uploaded".format(
                    dataset["lat"][0], box.current_lat
                ),
            )
            self.assertEqual(
                box.current_lon,
                dataset["lon"][0],
                "Instead of {}, a longitude of {} was uploaded".format(
                    dataset["lon"][0], box.current_lon
                ),
            )
            correct_sensor_title = app.config[
                "CO2LOGSERVER_OSEM_SENSOR_TITLE_GENERATOR"
            ](
                **app.config["CO2LOGSERVER_OSEM_COL_REGEX"]
                .search(wanted_col)
                .groupdict()
            )
            wrong_sensor_title = app.config[
                "CO2LOGSERVER_OSEM_SENSOR_TITLE_GENERATOR"
            ](
                **app.config["CO2LOGSERVER_OSEM_COL_REGEX"]
                .search(ignored_col)
                .groupdict()
            )
            self.assertIn(
                correct_sensor_title,
                box.sensors.by_title,
                "correct sensor '{}' was not created".format(
                    correct_sensor_title
                ),
            )
            self.assertNotIn(
                wrong_sensor_title,
                box.sensors.by_title,
                "wrong sensor '{}' was created".format(wrong_sensor_title),
            )
            sensor = box.sensors.by_title[correct_sensor_title]
            self.assertEqual(
                sensor.last_value,
                dataset[wanted_col][0],
                "Instead of {}, a measurement of {} was uploaded".format(
                    dataset[wanted_col][0], sensor.last_value
                ),
            )
            self.assertIn(
                "wanted",
                sensor.title,
                "Instead of something like 'wanted', "
                "the uploaded sensor has a title of '{}'".format(sensor.title),
            )

    @needs_osem_credentials
    def test_upload_rate_limit(self):
        app.config["CO2LOGSERVER_OSEM_MIN_SECONDS_BETWEEN_UPLOADS"] = 0
        dataset = {"sensortype_quantity_unit": [1], "lat": [45], "lon": [10]}
        response = self.upload_json(dataset)
        n_osem = response.json.get("osem-uploaded-measurements", 0)
        self.assertEqual(
            n_osem,
            1,
            "Instead of {} measurements, "
            "the server reported to have uploaded {} measurements "
            "to the OpenSenseMap".format(1, n_osem),
        )
        app.config["CO2LOGSERVER_OSEM_MIN_SECONDS_BETWEEN_UPLOADS"] = 60
        response = self.upload_json(dataset)
        n_osem = response.json.get("osem-uploaded-measurements", 0)
        self.assertEqual(
            n_osem,
            0,
            "The server should have uploaded no "
            "measurements to the OpenSenseMap, instead it reports to have "
            "uploaded {} measurements".format(n_osem),
        )


class DownloadTests(UploadTest):
    def test_download_home(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.app.get("/download")
        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_download_empty_DB_json(self):
        result = self.app.get("/download?format=json")
        self.assertEqual(result.data, b"[]")

    def test_download_DB_json(self):
        dataset_reference = {"co2": 500, "sensor_id": 1}
        dataset_to_upload = {"co2": [500], "sensor_id": [1]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get("/download?format=json")

        # the uploaded data is a subset of the response
        self.assertLessEqual(
            dataset_reference.items(), response.json[0].items()
        )

    def test_download_DB_json_selected_cols(self):
        dataset_reference = {"co2": 500, "sensor_id": 1}
        dataset_to_upload = {"co2": [500], "sensor_id": [1]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get("/download?format=json&cols=co2,sensor_id")

        self.assertDictEqual(response.json[0], dataset_reference)

    def test_download_DB_json_selected_cols_wrong_input(self):
        dataset_reference = {"co2": 500}
        dataset_to_upload = {"co2": [500], "sensor_id": [1]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get("/download?format=json&cols=co2,sensor_i")

        self.assertDictEqual(response.json[0], dataset_reference)

    def test_download_DB_json_numbers(self):
        dataset_reference = [
            {"co2": 500, "sensor_id": 1},
            {"co2": 800, "sensor_id": 2},
        ]
        dataset_to_upload = {"co2": [500, 800, 1000], "sensor_id": [1, 2, 3]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get(
            "/download?format=json&cols=co2,sensor_id&number=2"
        )

        # not used assertDictEqual because it is an array of dictionarys
        self.assertEqual(response.json, dataset_reference)

    def test_download_DB_json_numbers_wrong(self):
        dataset_reference = [
            {"co2": 500, "sensor_id": 1},
            {"co2": 800, "sensor_id": 2},
        ]
        dataset_to_upload = {"co2": [500, 800], "sensor_id": [1, 2]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get(
            "/download?format=json&cols=co2,sensor_id&number=a"
        )
        self.assertEqual(response.json, dataset_reference)

    def test_download_DB_json_max(self):
        dataset_reference = [
            {"co2": 500, "sensor_id": 1},
            {"co2": 600, "sensor_id": 3},
        ]
        dataset_to_upload = {"co2": [500, 800, 600], "sensor_id": [1, 2, 3]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get(
            "/download?format=json&cols=co2,sensor_id&max_co2=600"
        )
        self.assertEqual(response.json, dataset_reference)

    def test_download_DB_json_min(self):
        # Theorder is important
        dataset_reference = [
            {"co2": 800, "sensor_id": 2},
            {"co2": 600, "sensor_id": 3},
        ]
        dataset_to_upload = {"co2": [500, 800, 600], "sensor_id": [1, 2, 3]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get(
            "/download?format=json&cols=co2,sensor_id&min_co2=600"
        )
        self.assertEqual(response.json, dataset_reference)

    def test_download_DB_json_order_by_desc(self):
        # Theorder is important
        dataset_reference = [
            {"co2": 800, "sensor_id": 2},
            {"co2": 600, "sensor_id": 3},
            {"co2": 500, "sensor_id": 1},
        ]
        dataset_to_upload = {"co2": [500, 800, 600], "sensor_id": [1, 2, 3]}
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get(
            "/download?format=json&cols=co2,sensor_id&order_by=co2,desc"
        )
        self.assertEqual(response.json, dataset_reference)

    def test_download_DB_json_min_max_order_by_number(self):
        # Theorder is important
        dataset_reference = [
            {"co2": 800, "sensor_id": 2},
            {"co2": 600, "sensor_id": 3},
        ]
        dataset_to_upload = {
            "co2": [500, 800, 600, 1000],
            "sensor_id": [1, 2, 3, 2],
        }
        response_upload = self.upload_form(dataset_to_upload, salt=None)
        response = self.app.get(
            "/download?format=json&cols=co2,sensor_id&"
            "order_by=sensor_id,asc&min_co2=550&max_co2=900"
        )
        self.assertEqual(response.json, dataset_reference)
