# system modules
import unittest
import datetime
from urllib.parse import parse_qs
import hashlib

# internal modules
from co2logserver.utils import *

# external modules


class UtilsTest(unittest.TestCase):
    def test_table2dict(self):
        self.assertDictEqual(
            table2dict(header=["a", "b"], rows=[[1, 2], [3, 4]]),
            {"a": [1, 3], "b": [2, 4]},
        )

    def test_urlencode_multi_dict(self):
        d = {"a": ["1", "2", "3"], "b": ["4", "5", "6"]}
        self.assertEqual(d, parse_qs(urlencode_multi_dict(d)))

    def test_dictkeys2lower(self):
        d = {"ABC": 1, "Def": 2}
        self.assertDictEqual(dictkeys2lower(d), {"abc": 1, "def": 2})

    def test_unix_timestamp(self):
        self.assertEqual(
            unix_timestamp(
                datetime.datetime(1970, 1, 1, tzinfo=datetime.timezone.utc)
            ),
            0,
        )
        self.assertEqual(
            unix_timestamp(
                datetime.datetime(
                    2018, 7, 4, 14, 13, 44, tzinfo=datetime.timezone.utc
                )
            ),
            1530713624,
        )

    def test_datetime_from_unix_timestamp(self):
        self.assertEqual(
            utc_datetime_from_unix_timestamp(1530713624),
            datetime.datetime(
                2018, 7, 4, 14, 13, 44, tzinfo=datetime.timezone.utc
            ),
        )

    def test_unix_timestamp_consistency(self):
        self.assertEqual(
            utc_datetime_from_unix_timestamp(
                unix_timestamp(
                    datetime.datetime(
                        2018, 7, 4, 14, 13, 44, tzinfo=datetime.timezone.utc
                    )
                )
            ),
            datetime.datetime(
                2018, 7, 4, 14, 13, 44, tzinfo=datetime.timezone.utc
            ),
        )
        self.assertEqual(
            unix_timestamp(utc_datetime_from_unix_timestamp(1530713624)),
            1530713624,
        )

    def test_sqlsep(self):
        self.assertEqual(
            sqlsep([1234, "asdfasd", None]), "1234,'asdfasd',NULL"
        )

    def test_salted_checksum(self):
        self.assertEqual(
            salted_checksum("a", "b", "md5"),
            "187ef4436122d1cc2f40dc2b92f0eba0",
        )
        self.assertEqual(
            salted_checksum("a", "b", "sha1"),
            "da23614e02469a0d7c7bd1bdab5c9c474b1904dc",
        )
