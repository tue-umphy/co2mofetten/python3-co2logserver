# system modules
import os
import unittest
import itertools
import logging
import sqlite3

# internal modules
from co2logserver.database import DataBaseHandler, DATA_TABLE
from co2logserver.utils import *

logger = logging.getLogger("databasetest")


class DataBaseHandlerTest(unittest.TestCase):
    def setUp(self):
        self.dbfile = "{}.sqlite".format(self.__class__.__name__)
        self.db = DataBaseHandler(self.dbfile)

    def remove_dbfile(self):
        try:
            logger.debug("Removing test dbfile '{}'".format(self.dbfile))
            os.remove(self.dbfile)
        except FileNotFoundError:
            pass

    def tearDown(self):
        self.assert_other_connection_sees_same_test_table()
        self.remove_dbfile()

    @property
    def dbfile_size(self):
        try:
            return os.path.getsize(self.dbfile)
        except FileNotFoundError:
            return -1

    def assert_dbfile_exists(self, msg=None):
        self.assertTrue(
            os.path.isfile(self.dbfile),
            "database file does not exist as expected" if msg is None else msg,
        )

    def assert_dbfile_not_exists(self, msg=None):
        self.assertFalse(
            os.path.isfile(self.dbfile),
            "database file exist unexpectedly" if msg is None else msg,
        )

    def assert_dbfile_empty(self, msg=None):
        self.assert_dbfile_exists()
        self.assertEqual(
            self.dbfile_size,
            0,
            "database file is unexpectedly non-empty" if msg is None else msg,
        )

    def assert_dbfile_nonempty(self, msg=None):
        self.assert_dbfile_exists()
        self.assertGreater(
            self.dbfile_size,
            0,
            "database file is expectedly empty" if msg is None else msg,
        )

    def assert_other_connection_sees_same_test_table(self):
        sql = "SELECT * FROM {table}".format(table=DATA_TABLE)
        cursor_1 = self.db.connection.execute(sql)
        conn_2 = sqlite3.connect(self.dbfile)
        cursor_2 = conn_2.execute(sql)
        try:
            for r1, r2 in itertools.zip_longest(cursor_1, cursor_2):
                self.assertIsNot(
                    r1,
                    None,
                    "Separate connection to database "
                    "file unexpectedly yields more results",
                )
                self.assertIsNot(
                    r2,
                    None,
                    "Separate connection to database "
                    "file unexpectedly yields less results",
                )
                self.assertEqual(
                    r1,
                    r2,
                    "Separate connection to "
                    "database file unexpectedly yields different results",
                )
        finally:
            conn_2.close()

    def test_tablecursor_accessor_creates_table(self):
        self.db.tablecursor
        self.assertGreater(len(self.db.header), 0)
        self.assert_dbfile_nonempty()

    def test_header_accessor_creates_table(self):
        self.assertGreater(len(self.db.header), 0)
        self.assert_dbfile_nonempty()

    def test_insert_into_table(self):
        data_new = {"co2": 1, "hum": 0.3, "pres": 1234}
        self.db.insert_into_table(
            table=DATA_TABLE, columns=data_new.keys(), values=data_new.values()
        )
        self.assertEqual(len(set(data_new.keys()) - set(self.db.header)), 0)
        header, cursor = self.db.select_from_table(columns=data_new.keys())
        data_in = table2dict(header, cursor)
        self.assertDictEqual(data_new, {k: v[0] for k, v in data_in.items()})

    def test_save_records(self):
        data_new = {"co2": [1, 2, 3], "hum": [1, 2.3, 1.2], "pres": [4, 3, 2]}
        self.db.save_records(data_new)
        header, cursor = self.db.select_from_table(columns=data_new.keys())
        data_in = table2dict(header, cursor)
        self.assertDictEqual(data_new, data_in)
