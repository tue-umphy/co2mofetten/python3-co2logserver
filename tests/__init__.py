# system modules
import os
import unittest

# internal modules
from co2logserver import app

# external modules

# decorator to skip tests that require credentials


def needs_osem_credentials(f):
    return unittest.skipUnless(
        (
            app.config["CO2LOGSERVER_OSEM_USERNAME"]
            or app.config["CO2LOGSERVER_OSEM_EMAIL"]
        )
        and app.config["CO2LOGSERVER_OSEM_PASSWORD"],
        "no OpenSenseMap credentials available in config",
    )(f)
