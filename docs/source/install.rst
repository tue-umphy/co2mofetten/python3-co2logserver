
Installation
============

:mod:`co2logserver` is best installed via ``pip``::

    pip3 install --user co2logserver
